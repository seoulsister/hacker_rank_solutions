## HackerRank Solutions

* Mean, Median, and Mode
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-basic-statistics/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/mean_median_mode.py)

* Weighted Mean
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-weighted-mean/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/weighted_mean.py)

* Quartiles
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-quartiles/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/quartiles.py)

* Interquartile Range
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-interquartile-range/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/interquartile_range.py)

* Standard Deviation
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-standard-deviation/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/standard_deviation.py)

* Binomial Distribution 1 
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-binomial-distribution-1/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/binomial_distribution1.py)

* Binomial Distribution 2
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-binomial-distribution-2/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/binomial_distribution2.py)

* Geometric Distribution 1
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-geometric-distribution-1/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/geometric1.py)

* Geometric Distribution 2
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-geometric-distribution-2/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/geometric2.py)
 
* Poisson Distribution 1
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-poisson-distribution-1/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/poisson1.py)

* Poisson Distribution 2
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-poisson-distribution-2/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/poisson2.py)


* Normal Distribution 1
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-normal-distribution-1/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/normal1.py)

 * Normal Distribution 2
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-normal-distribution-2/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/normal2.py)


 * Central Limit Theorem 1
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-the-central-limit-theorem-1/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/central_limit_theorem1.py)

 * Central Limit Theorem 2
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-normal-distribution-2/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/central_limit_theorem2.py)

 * Central Limit Theorem 3
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-the-central-limit-theorem-3/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/central_limit_theorem3.py)

* Pearson's Correlation Coefficient
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-pearson-correlation-coefficient/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/pearsons1.py)

* Spearman's Rank Correlation Coefficient
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-spearman-rank-correlation-coefficient/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/spearmans.py)

* Least Squares Regression Line
    * [Problem Statement](https://www.hackerrank.com/challenges/s10-least-square-regression-line/problem)
    * [Solution](https://gitlab.com/seoulsister/hacker_rank_solutions/blob/master/ten_days_of_statistics/linear_reg.py)
























