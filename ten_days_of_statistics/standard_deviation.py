# Enter your code here. Read input from STDIN. Print output to STDOUT
import statistics
def standard_deviation(arr):
    sum = 0
    mean = statistics.mean(arr)
    for num in arr:
        sum += (num-mean)**2
    stdev = (sum/len(arr))**0.5
    return round(stdev, 1)

n = input()
nums = [int(n) for n in input().split()]
print(standard_deviation(nums))
