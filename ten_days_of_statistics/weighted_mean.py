import itertools
def weighted_mean(data):
    nums = data[0]
    weights = data[1]
    total = 0
    for num, weight in zip(nums, weights):
        total += num*weight
    print(round(float(total)/float(sum(weights)), 1))


n = int(input())
data = []
for i in range(1,3):
    data.append([int(i) for i in input().split()])
weighted_mean(data)