# Enter your code here. Read input from STDIN. Print output to STDOUT

def get_expected_value(lamb):
    expected_value = lamb + lamb**2
    return expected_value

cost_a = 160.0+ 40.0*(get_expected_value(0.88))
cost_b = 128.0 + 40.0*(get_expected_value(1.55))
print(round(cost_a,3))
print(round(cost_b, 3))