# Enter your code here. Read input from STDIN. Print output to 
import math 
def mean(arr):
    num_items = float(len(arr))
    sum_items = float(sum([x for x in arr]))
    return sum_items/num_items

def median(arr):
    num_items = len(arr)
    if num_items % 2 != 0:
        middle = (num_items + 1)/2
        return arr[middle]
    else:
        middle = float((num_items + 1)/2)
        return (arr[int(middle-0.5)] + arr[int(middle+0.5)])/float(2)
def mode(arr):
    return max(arr, key=arr.count)

n = int(raw_input().strip());
data = map(int, raw_input().strip().split(' '));
data.sort();

# arrsize = len(data);
print mean(data);
print median(data);
print mode(data);

