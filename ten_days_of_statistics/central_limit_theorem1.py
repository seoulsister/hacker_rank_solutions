# Enter your code here. Read input from STDIN. Print output to STDOUT
from math import erf, sqrt
def compute_prob(x, mu, sigma):
    return (1.0 + erf((x- mu)/ (sigma*sqrt(2.0)))) / 2.0

n = 49
mu = 205
sigma = 15
y = 9800

z  = (y - n*mu)/(n**(0.5)*sigma)
print(round(1 - compute_prob(abs(z), 0, 1), 4))

