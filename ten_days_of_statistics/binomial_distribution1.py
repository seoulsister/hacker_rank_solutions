from math import factorial

b = 1.09
g = 1
p = b/(b+g)
q = 1-p
n = 6
x = 3
sum = 0
for i in range(x, n+1):
   sum += factorial(n)/(factorial(i)*factorial(n-i))* p**i * q**(n-i)
print(round(sum, 3))