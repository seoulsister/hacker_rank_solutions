n = input()
x = input().split()
y = input().split()

x_sorted = sorted([float(n) for n in x])
y_sorted = sorted([float(n) for n in y])
set_x = [x_sorted.index(float(n))+1 for n in x]
set_y = [y_sorted.index(float(n))+1 for n in y]
sum_squared_diffs = sum([(set_x[i]-set_y[i])**2 for i in range(len(set_x))])

n = 10
r = 6.0*sum_squared_diffs/(n*(n**2-1))
print(round(1-r, 3))