import statistics
def median(arr):
    return statistics.median(arr)

def split_list(arr): 
    arr = sorted(arr)  
    if(len(arr)%2!=0):
        med = median(arr)
        med_index = arr.index(med)
        arr = [arr[i] for i in range(len(arr)) if i!=arr.index(med)]   
    else:
        arr = arr
    half = len(arr)//2
    return arr[:half],arr[half:]

def get_quartiles(arr):
    upper, lower = split_list(arr)[0], split_list(arr)[1]
    first = int(median(upper))
    second = int(median(arr))
    third = int(median(lower))
    return first, second, third

n = input()
nums = input()
data = [int(n) for n in nums.split()]
first, second, third = get_quartiles(data)
print(first) 
print(second)
print(third)