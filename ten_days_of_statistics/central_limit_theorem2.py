# Enter your code here. Read input from STDIN. Print output to STDOUT
from math import erf, sqrt
def compute_prob(x, mu, sigma):
    return (1.0 + erf((x- mu)/ (sigma*sqrt(2.0)))) / 2.0

n = 100
mu = 2.4
sigma = 2.0
y = 250

z  = (y - n*mu)/(n**(0.5)*sigma)
print(round(compute_prob(abs(z), 0, 1), 4))