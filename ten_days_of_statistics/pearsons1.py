import statistics
def pearson_corr(x, y, num):
    mu_x = statistics.mean(x)
    mu_y = statistics.mean(y)
    sigma_x = statistics.pstdev(x)
    sigma_y = statistics.pstdev(y)
    a = [(n1-mu_x) for n1 in x]
    b = [(n2-mu_y) for n2 in y]
    return round(sum([a[i]*b[i] for i in range(len(b))])/(num*sigma_x*sigma_y), 3)


n = float(input())
set_x = [float(n) for n in input().split()]
set_y = [float(n) for n in input().split()]
print(pearson_corr(set_x, set_y, n))