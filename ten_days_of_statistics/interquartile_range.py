# Enter your code here. Read input from STDIN. Print output to STDOUT

import statistics
def median(arr):
    return statistics.median(arr)

def split_list(arr): 
    arr = sorted(arr)  
    if(len(arr)%2!=0):
        med = median(arr)
        med_index = arr.index(med)
        arr = [arr[i] for i in range(len(arr)) if i!=arr.index(med)]   
    else:
        arr = arr
    half = len(arr)//2
    return arr[:half],arr[half:]

def get_quartiles(arr):
    upper, lower = split_list(arr)[0], split_list(arr)[1]
    first = int(median(upper))
    second = int(median(arr))
    third = int(median(lower))
    return first, second, third

def get_interquartile_range(nums, freqs):
    nums_w_freqs = [nums[i]*int(freqs[i]) for i in range(len(nums))]
    num_arrays = [nums_w_freqs[i].split() for i in range(len(nums_w_freqs))]    
    final_nums = []
    for arr in num_arrays:
        final_nums += arr
    final_nums = [int(n) for n in final_nums]
    first, second, third = get_quartiles(final_nums)
    return float(third - first)

n = input()
nums = [x+" " for x in input().split()]
freqs = input().split()
print(get_interquartile_range(nums, freqs))
