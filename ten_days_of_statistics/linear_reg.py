# Enter your code here. Read input from STDIN. Print output to STDOUT
import statistics
from sklearn import linear_model
import numpy as np
x_1 = [95, 85, 80, 70, 60]
y = [85, 95, 70, 65, 70]
x = np.asarray(x_1).reshape(-1, 1)
lm = linear_model.LinearRegression()
lm.fit(x, y)
b = lm.coef_[0]
x_mu = statistics.mean(x_1)
y_mu = statistics.mean(y)
a = y_mu - b*x_mu
print(round(a + 80*b, 3))