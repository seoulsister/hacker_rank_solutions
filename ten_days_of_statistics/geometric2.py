# Enter your code here. Read input from STDIN. Print output to STDOUT

p = 1.0/3.0
q = (1-p)
n = 5
result = 0
for i in range(1, n+1):
    result += q**(i-1)*p
print(round(result, 3))