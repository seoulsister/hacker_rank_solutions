from math import factorial

p = 0.12
q = 1-p
n = 10
x = 2
sum1 = 0
for i in range(0, x+1):
   sum1 += factorial(n)/(factorial(i)*factorial(n-i))* p**i * q**(n-i)
print(round(sum1, 3))

sum2 = 0
for i in range(x, n+1):
   sum2 += factorial(n)/(factorial(i)*factorial(n-i))* p**i * q**(n-i)
print(round(sum2, 3))
