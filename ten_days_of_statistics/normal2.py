# Enter your code here. Read input from STDIN. Print output to STDOUT
from math import erf, sqrt
def compute_prob(x, mu, sigma):
    return (1.0 + erf((x- mu)/ (sigma*sqrt(2.0)))) / 2.0

mu = 70
sigma= 10
var = sigma**2

print(round(100 - compute_prob(80, mu, sigma)*100,2))
print(round(100 - compute_prob(60, mu, sigma)*100, 2))
print(round(compute_prob(60, mu, sigma)*100, 2))