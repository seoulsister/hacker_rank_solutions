# Enter your code here. Read input from STDIN. Print output to STDOUT
from math import erf, sqrt
def compute_prob(x, mu, sigma):
    return (1.0 + erf((x- mu)/ (sigma*sqrt(2.0)))) / 2.0

mu = 20
sigma= 2
var = sigma**2
x = 19.5
a = 20
b = 22
print(compute_prob(x, mu, sigma))
print(compute_prob(b, mu, sigma) - compute_prob(a, mu, sigma))